from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .models import Courses
from .views import course_list, create_course, course_detail, delete_course, register, loginPage, logoutPage

# Create your tests here.
class Coursetest(TestCase):
    def setUp(self):
        self.courses = Courses.objects.create(matkul="a", dosen="b", sks="3", deskripsi="d", semester="5", ruang="f")

#Check database
    def test_amount(self):
        self.assertEqual(Courses.objects.all().count(), 1)

#Check URL
    def test_index_url_exist(self):
        response = Client().get("/")
        self.assertEqual(response.status_code, 200)

    def test_login_url_exist(self):
        response = Client().get("/login/")
        self.assertEqual(response.status_code, 200)

    def test_register_url_exist(self):
        response = Client().get("/register/")
        self.assertEqual(response.status_code, 200)

#Check Views
    def test_course_list(self):
        response = resolve("/")
        self.assertEqual(response.func, course_list)
    
    def test_add_course(self):
        response = resolve("/addcourse/")
        self.assertEqual(response.func, create_course)

    def test_course_detail(self):
        response = resolve("/coursedetail/1/")
        self.assertEqual(response.func, course_detail)

    def test_delete_course(self):
        response = resolve("/deletecourse/1/")
        self.assertEqual(response.func, delete_course)

    def test_register(self):
        response = resolve("/register/")
        self.assertEqual(response.func, register)

    def test_login(self):
        response = resolve("/login/")
        self.assertEqual(response.func, loginPage)

    def test_logout(self):
        response = resolve("/logout/")
        self.assertEqual(response.func, logoutPage)

    def test_create_course(self):
        args = {'matkul':'b', 'dosen':'c', 'sks':'2', 'deskripsi':'mihoyo', 'semester':'4', 'ruang':'r'}
        self.client.post('/addcourse/', data=args)
        self.assertEquals(Courses.objects.all().count(), 2)
