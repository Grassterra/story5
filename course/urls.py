from django.urls import path

from . import views

app_name = 'course'

urlpatterns = [
    path('', views.course_list, name='index'),
    path('addcourse/', views.create_course, name='add'),
    path('coursedetail/<str:key>/', views.course_detail, name='detail'),
    path('deletecourse/<str:key>/', views.delete_course, name='delete'),
    path('register/', views.register, name='register'),
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutPage, name='logout')
]