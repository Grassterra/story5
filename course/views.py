from django.shortcuts import render, redirect
from .forms import courseform, CreateUserForm
from .models import Courses
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout

# Create your views here.

def course_list(request):
    courses = Courses.objects.all()
    context = {"obj" : courses}
    return render(request, "course/index.html", context)

def create_course(request):
    form = courseform()
    if request.method == 'POST':
        form = courseform(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/")

    context = {"form" : form}
    return render (request,"course/addcourse.html", context)

def course_detail(request, key):
    myid = Courses.objects.get(id=key)
    context = {"obj" : myid}
    return render(request, "course/coursedetail.html", context)

def delete_course(request, key):
    myid = Courses.objects.get(id=key)
    myid.delete()
    return redirect("/")

def register(request):
    form = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("/admin/login/?next=/")

    context = {'form':form}
    return render(request, "course/register.html", context)

def loginPage(request):
    if request.user.is_authenticated:
        return redirect("/")

    else:
        if request.method == "POST":
            username = request.POST.get("username")
            password = request.POST.get("password")

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("/")

        context = {}
        return render(request, "course/login.html", context)

def logoutPage(request):
    logout(request)
    return redirect("/")
