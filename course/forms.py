from django import forms
from .models import Courses
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from crispy_forms.helper import FormHelper


class courseform(forms.ModelForm):    
    class Meta:
        model = Courses
        fields = (
            "matkul",
            "dosen",
            "sks",
            "deskripsi",
            "semester",
            "ruang",
            )

class CreateUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2']