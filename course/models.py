from django.db import models

# Create your models here.
class Courses(models.Model):
    matkul = models.CharField(max_length=100, default="matkul")
    dosen = models.CharField(max_length=50, default="dosen")
    sks = models.IntegerField(default="3")
    deskripsi = models.TextField(null = True)
    semester = models.CharField(max_length=50, default="Gasal")
    ruang = models.CharField(max_length=50, null = True)

def __str__(self):
    return self.matkul
