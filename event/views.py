from django.shortcuts import render, redirect
from django.forms import inlineformset_factory
from .models import Event,Participant
from .forms import Participantform, Eventform

# Create your views here.

def event_list(request):
    event = Event.objects.all()
    participant = Participant.objects.all()
    form = Eventform()
    if request.method == 'POST':
        form = Eventform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('event:index')
    context = {"evt":event, "ptc":participant, "form":form}
    return render(request, "event/index.html", context)

def event_register(request, key):
    key = Event.objects.get(id = key)
    form = Participantform(initial={'event_registered':key})
    if request.method == 'POST':
        form = Participantform(request.POST)
        if form.is_valid():
            form.save()
            return redirect('event:index')

    context = {"form":form}
    return render(request, "event/register.html", context)

def event_delete(request, key):
    key = Event.objects.get(id=key)
    key.delete()
    return redirect('event:index')