from django.db import models

# Create your models here.
class Event(models.Model):
    name = models.CharField(max_length=50)

    
class Participant(models.Model):
    name = models.CharField(max_length=50)
    event_registered = models.ForeignKey(Event, on_delete=models.CASCADE)