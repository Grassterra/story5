from django.urls import path

from . import views

app_name = 'event'

urlpatterns = [
    path('', views.event_list, name='index'),
    path('register/<str:key>/', views.event_register, name='register'),
    path('deleted/<str:key>/', views.event_delete, name='delete'),
]