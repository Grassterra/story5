from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Event, Participant

class Participantform(forms.ModelForm):    
    class Meta:
        model = Participant
        fields = ("name", "event_registered")
        widgets = {'event_registered': forms.HiddenInput()}

class Eventform(forms.ModelForm):    
    class Meta:
        model = Event        
        fields = ("name",)
        labels = {"name":_("Add new activity")}