from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .models import Event, Participant
from .views import event_list, event_register, event_delete

# Create your tests here.
class Eventtest(TestCase):
    def setUp(self):
        self.event = Event.objects.create(name = "panen boba")
        Participant.objects.create(name = "izuri", event_registered = self.event)
        Participant.objects.create(name = "bobi bola", event_registered = self.event)
        Participant.objects.create(name = "keranjang", event_registered = self.event)

#Check Database
    def test_participants_amount(self):
        self.assertEqual(Participant.objects.all().count(),3)

    def test_Events_amount(self):
        self.assertEqual(Event.objects.all().count(),1)

#Check URL
    def test_index_url_exist(self):
        response = Client().get("/events/")
        self.assertEqual(response.status_code, 200)

    def test_register_url_exist(self):
        response = Client().get("/events/register/1/")
        self.assertEqual(response.status_code, 200)

#Check Views
    def test_list_views_exist(self):
        response = resolve("/events/")
        self.assertEqual(response.func, event_list)

    def test_list_views_post(self):
        response = self.client.post("/events/", data={"name": "Test"})
        self.assertEqual(response["location"], "/events/")

    def test_register_views_exist(self):
        response = resolve("/events/register/1/")
        self.assertEqual(response.func, event_register)