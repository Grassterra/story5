from django.urls import path
from . import views

from . import views

app_name = 'library'

urlpatterns = [
    path('', views.index, name='index'),
    path("books/", views.return_data, name="books"),
]