from django.test import TestCase, Client

# Create your tests here.
class UnitTest(TestCase):
    def test_url(self):
        response = Client().get('/library/')
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        response = self.client.get("/library/")
        self.assertTemplateUsed(response, "library/index.html")

    def test_api_google(self):
        response = self.client.get("/library/books/?q=pokemon")
        self.assertEqual(response.status_code, 200)
        content = response.content.decode("utf-8")
        self.assertIn("items", content)
        self.assertEqual(response['content-type'], 'application/json')

