from django.shortcuts import render, HttpResponse
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, "library/index.html")

def return_data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    data = requests.get(url)
    books = json.loads(data.content)
    return JsonResponse(books, safe=False)
